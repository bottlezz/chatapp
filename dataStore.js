function DataStore(){
  this.history=[
  {dateUtc: 1454643934270 , data:{user:'Greg', msg:'hello'}},
  {dateUtc: 1454643935270 , data:{user:'Jinny', msg:'hello'}},
  {dateUtc: 1454643936270 , data:{user:'David', msg:'hello'}}
  ];



    this.get=function(starts,ends){
      var msgs = [];
      for(var i=0;i<this.history.length;i++){
        if(this.history[i].dateUtc < ends && this.history[i].dateUtc > starts){
          msgs.push(this.history[i]);
        }
      }
      return msgs;
    };
    this.set=function(data){
      this.history.push({dateUtc:Date.now(), data:data});
      return;
    };

}
module.exports = new DataStore();
