//import libraries.
var http = require('http');
var fs = require('fs');
var url = require('url');

//import datastore
var dataStore = require('./DataStore.js');
//peak whats inside datastore
console.log(dataStore.get(Date.now()));

//carete http server.
var server=http.createServer(function(req,res){
  var parsedUrl = url.parse(req.url,true);
  console.log(parsedUrl.pathname);

  //handle getMsg
  if(parsedUrl.pathname=='/getMsg'){
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(dataStore.get(parsedUrl.query.starts,parsedUrl.query.ends)));
    return;
  }

  //handle sendMsg
  if(parsedUrl.pathname=='/sendMsg'){
    dataStore.set(parsedUrl.query);
    res.writeHead(200);
    res.end(JSON.stringify(true));
    return;
  }
  //server sreve file
  fs.readFile(__dirname + '/public'+req.url, function (err,data) {
    if (err) {
      res.writeHead(404);
      res.end(JSON.stringify(err));
      return;
    }
    res.writeHead(200);
    res.end(data);
  });

});
//server runs on port 8080
server.listen(8080);
